This is a small script that wraps the texlive installer so that it forces vanilla texlive onto the system.

It creates a /usr/local/texlive directory and hands out permissions for this. For this sudo is required.

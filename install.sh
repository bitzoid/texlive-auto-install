#!/bin/bash

tl="/usr/local/texlive"

echo "[install.sh] You may have to enter your root password once or twice. I need it to set up $tl and will drop root privileges afterwards."

if [ ! -d "$tl" ]; then
  sudo mkdir -P "$tl"
fi

targetug="$(id -u):$(id -g)"

if [ "$(stat -c '%U:%G' "$tl")" != "$targetug" ]; then
  sudo chown -R "$targetug" "$tl"
fi

wget --no-clobber http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz
tar -xzf install-tl-unx.tar.gz
cd install-tl*/
wget --no-clobber http://ftp.gwdg.de/pub/ctan/systems/texlive/tlnet/tlpkg/texlive.tlpdb

./install-tl --profile=texlive.tlpdb

echo
echo "[install.sh] Installed texlive to $tl."
echo

bp="/usr/local/texlive/$(date +%Y)/bin/x86_64-linux"

echo "[install.sh] Adding the new path $bp to \$PATH in your ~/.bashrc. You might want to move this to a different file, perhaps."

echo "export PATH=\"$bp:\$PATH\"" >> .bashrc

echo "[install.sh] Done. Please open a new shell and have fun with a fresh latex installation. Remember to update it from time to time with"
echo
echo "  tlmgr update --self --all"
echo
